$(document).ready(function(){
    history.pushState({page: 1}, "title 1", "?page=1");
    PopUpHide();
});


let Name, Email, Comment;

function PopUpShow(){
    getValue();
    history.pushState({page: 2}, "title 2", "?page=2");
    $("#popup1").show();
}

function getValue(){
    Name = localStorage.getItem('name');
    Email = localStorage.getItem('email');
    Comment = localStorage.getItem('comment');
    console.log(Name);
    console.log(Email);
    console.log(Comment);
    if (Name !==undefined && Email !==undefined && Comment!==undefined) {
        document.getElementById("name").value = Name;
        document.getElementById("e-mail").value = Email;
        document.getElementById("comment").value = Comment;
    }else {
        document.getElementById("name").value = "";
        document.getElementById("e-mail").value = "";
        document.getElementById("comment").value = "";
    }
}

function PopUpHide(){
    $("#popup1").hide();
}

window.addEventListener('popstate', function(){
    PopUpHide();
    localStorage.setItem('name', document.getElementById("name").value);
    localStorage.setItem('email', document.getElementById("e-mail").value);
    localStorage.setItem('comment', document.getElementById("comment").value);
    History.go(-1);
});
